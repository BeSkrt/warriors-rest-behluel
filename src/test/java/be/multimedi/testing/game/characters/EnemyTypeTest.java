package be.multimedi.testing.game.characters;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class EnemyTypeTest {
    private EnemyType skeleton;
    private EnemyType goblin;

    @BeforeEach
    void init() {
        skeleton = EnemyType.SKELETON;
        goblin = EnemyType.GOBLIN;
    }

    @Test
    void getHealth() {
        assertEquals(20, skeleton.getHealth());
        assertEquals(22, goblin.getHealth());
    }

    @Test
    void getMaxDamage() {
        assertEquals(25, skeleton.getMaxDamage());
        assertEquals(20, goblin.getMaxDamage());
    }

    @Test
    void getRandomType() {
        EnemyType randomEnemy = EnemyType.getRandomType();

        assertEquals(randomEnemy, EnemyType.getRandomType());
    }

    @AfterEach
    void destroy() {
        skeleton = null;
        goblin = null;
    }
}