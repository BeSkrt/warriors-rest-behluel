package be.multimedi.testing.game.logic;

import be.multimedi.testing.game.characters.Player;

import java.util.Scanner;

/**
 * @author Sven Wittoek
 * created on Wednesday, 2/10/2021
 */
public class Game {
    private Scanner scanner;
    private Player player;
    private int choice;
    private GameLogic gameLogic;

    public Game() {
        scanner = new Scanner(System.in);
    }

    Game(Scanner scanner, Player player, GameLogic gameLogic) {
        this.scanner = scanner;
        this.player = player;
        this.gameLogic = gameLogic;
    }

    Game(Player player, GameLogic gameLogic) {
        this.player = player;
        this.gameLogic = gameLogic;
    }

    public Scanner getScanner() {
        return scanner;
    }

    void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public Player getPlayer() {
        return player;
    }

    void setPlayer(Player player) {
        this.player = player;
    }

    public int getChoice() {
        return choice;
    }

    void setChoice(int choice) {
        this.choice = choice;
    }

    public GameLogic getGameLogic() {
        return gameLogic;
    }

    public void setGameLogic(GameLogic gameLogic) {
        this.gameLogic = gameLogic;
    }

    public void play() {
        askChoice();

        while (choice != 2) {

            startUpGame();

            startFightingSequence();

            closeDownGame();

        }

    }

    private void askChoice() {
        try {
            System.out.println("What would you like to do?");
            System.out.printf(
                    "1. Start game %n" +
                    "2. Quit game %n");

            choice = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException nfe) {
            System.out.println("Please enter a number!");
            askChoice();
        }
    }

    private void startFightingSequence() {
        choice = 0;
        while (player.getHealth() > 0 && choice != 2) {
            gameLogic.commenceCombat();
            if (player.getHealth() > 0) {
                askDecision();

                if (choice == 1) {
                    System.out.println("You dive deeper into the dungeon, hoping to find a brilliant treasure.");
                }
            }
        }
    }

    void askDecision() {
        try {
            System.out.printf("What would you like to do? %n" +
                    "1. Explore further %n" +
                    "2. Escape dungeon %n");
            choice = Integer.parseInt(scanner.nextLine());
        } catch (NumberFormatException nfe) {
            System.out.println("Please enter a number!");
            askDecision();
        }
    }

    void closeDownGame() {
        if (player.getHealth() <= 0) {
            System.out.println("That last attack was way more than you can take and you collapse to the floor of the " +
                    "dungeon, thinking you just took your last breath.");
            System.out.println("You blink one last time before everything goes black and you see the enemy drawing his" +
                    " weapon for a last time. You turn your head away and feel warm blood spreading over your body.");
        } else {
            System.out.println("You managed to find the exit and you leave the dungeon with some extra gold, making plans" +
                    " to spend it al at the local inn.");
        }
        askChoice();
    }

    void startUpGame() {
        if (player == null) {
            System.out.println("What is your name?");
            String name = scanner.nextLine();
            this.player = new Player(name, 120, 50, 5);
            gameLogic = new GameLogic(player);
        } else {
            player.heal(player.getMaxHealth());
            player.throwAwayAllPotions();
            player.addPotions(5);
        }
        System.out.println("You enter a cave and get attacked");
    }
}
