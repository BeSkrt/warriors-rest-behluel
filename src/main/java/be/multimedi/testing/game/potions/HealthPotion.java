package be.multimedi.testing.game.potions;

import be.multimedi.testing.game.characters.Player;

/**
 * @author Sven Wittoek
 * created on Monday, 2/8/2021
 */
public enum HealthPotion implements Drinkable{
    SMALL_HEALTH_POTION(30),
    BIG_HEALTH_POTION(60);

    protected int healAmount;

    HealthPotion(int healAmount){
        this.healAmount = healAmount;
    }

    public int getHealAmount() {
        return healAmount;
    }


    @Override
    public void drink(Player player) {
        player.heal(getHealAmount());
    }
}
