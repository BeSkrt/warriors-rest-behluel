package be.multimedi.testing.game.characters;

import be.multimedi.testing.game.potions.Drinkable;
import be.multimedi.testing.game.potions.HealthPotion;
import nl.jqno.equalsverifier.EqualsVerifier;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

class PlayerTest {
    private Player player;
    private Collection<Drinkable> availablePotions;

    @BeforeEach
    void init() {
        player = new Player("Behlül", 100, 50);
        availablePotions = new ArrayList<>();
    }

    @Test
    void getName() {
        assertEquals("Behlül", player.getName());
    }

    @Test
    void getMaxHealth() {
        assertEquals(100, player.getMaxHealth());
    }

    @Test
    void getHealth() {
        player.setHealth(50);

        assertEquals(50, player.getHealth());
    }

    @Test
    void getMaxDamage() {
        assertEquals(50, player.getMaxDamage());
    }

    @Test
    @Disabled
    void getRandomDamage() { // niet zeker of dit überhaupt getest moet worden aangezien de waarden random zijn?
        int randomDamage = player.getRandomDamage();

        assertEquals(randomDamage, player.getRandomDamage());
    }

    @Test
    void getLevel() {
        player.setLevel(1);

        assertEquals(1, player.getLevel());
    }

    @Test
    void getAvailablePotions() {
        availablePotions.add(HealthPotion.SMALL_HEALTH_POTION);
        availablePotions.add(HealthPotion.BIG_HEALTH_POTION);

        player.setAvailablePotions(availablePotions);

        assertEquals(availablePotions, player.getAvailablePotions());
    }

    @Test
    void getNumberOfUsedPotions() {
        player.setNumberOfUsedPotions(5);

        assertEquals(5, player.getNumberOfUsedPotions());
    }

    @Test
    void heal() {
        availablePotions.add(HealthPotion.SMALL_HEALTH_POTION);
        player.setHealth(20);
        player.heal(HealthPotion.SMALL_HEALTH_POTION.getHealAmount());

        assertEquals(50, player.getHealth());
    }

    @Test
    void takeDamage() {
        int damage = 20;

        player.setHealth(100);
        player.takeDamage(damage);

        assertEquals(80, player.getHealth());
    }

    @Test
    void addPotion() {
        availablePotions.add(HealthPotion.SMALL_HEALTH_POTION);

        assertEquals(1, availablePotions.size());
    }

    @Test
    void addPotions() {
        int amount = 5;

        for(int i = 0; i < amount; i++) {
            availablePotions.add(HealthPotion.SMALL_HEALTH_POTION);
        }

        assertEquals(5, availablePotions.size());
    }

    @Test
    void throwAwayAllPotions() {
        availablePotions.add(HealthPotion.SMALL_HEALTH_POTION);
        availablePotions.clear();

        assertTrue(availablePotions.isEmpty());
    }

    @Test
    void incrementNumberOfUsedPotions() {
        player.incrementNumberOfUsedPotions();

        assertEquals(1, player.getNumberOfUsedPotions());
    }

    @Test
    void takeOutPotion() {
        availablePotions.add(HealthPotion.SMALL_HEALTH_POTION);
        availablePotions.remove(HealthPotion.SMALL_HEALTH_POTION);

        assertTrue(availablePotions.isEmpty());
    }

    @Test
    void testEqualsAndHashCode() {
        EqualsVerifier.simple().forClass(Player.class).verify();
    }

    @AfterEach
    void destroy() {
        player = null;
        availablePotions = null;
    }
}