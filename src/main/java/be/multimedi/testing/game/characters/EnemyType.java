package be.multimedi.testing.game.characters;

import java.util.Random;

/**
 * @author Sven Wittoek
 * created on Friday, 2/5/2021
 */
public enum EnemyType {
    SKELETON(20, 25),
    GOBLIN(22, 20);

     final int HEALTH;
     final int MAX_DAMAGE;

    EnemyType(int health, int maxDamage){
        this.HEALTH = health;
        this.MAX_DAMAGE = maxDamage;
    }

    public int getHealth() {
        return HEALTH;
    }

    public int getMaxDamage() {
        return MAX_DAMAGE;
    }

    public static EnemyType getRandomType(){
        Random random = new Random();
        return EnemyType.values()[random.nextInt(EnemyType.values().length - 1)];
    }
}
