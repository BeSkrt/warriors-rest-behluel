package be.multimedi.testing.game.logic;

import be.multimedi.testing.game.characters.Player;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Random;
import java.util.Scanner;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class GameLogicTest {
    private GameLogic gameLogic;
    @Mock
    private Player player;
    @Mock
    private Combat combat;
    @Mock
    private InputStreamReader reader;

    @BeforeEach
    void init() {
        player = new Player("Behlül", 100, 50);
        gameLogic = new GameLogic(player);
    }

    @Test
    void getScanner() {
        Scanner scanner = new Scanner(System.in);

        gameLogic.setScanner(scanner);

        assertEquals(scanner, gameLogic.getScanner());
    }

    @Test
    void getRandom() {
        Random random = new Random();

        gameLogic.setRandom(random);

        assertEquals(random, gameLogic.getRandom());
    }

    @Test
    void getChoice() {
        int choice = 2;

        gameLogic.setChoice(choice);

        assertEquals(choice, gameLogic.getChoice());
    }

    @Test
    void getPlayer() {
        gameLogic.setPlayer(player);

        assertEquals(player, gameLogic.getPlayer());
    }

    @Test
    void getCombat() {
        combat = Combat.getInstance();

        gameLogic.setCombat(combat);

        assertEquals(combat, gameLogic.getCombat());
    }

    @Test
    void commenceCombat() {

    }

    @Test
    void drinkPotion() {
    }

    @Test
    void fight() {
    }

    @Test
    void decideAction() {
    }

    @Test
    void createEnemy() {
    }
}