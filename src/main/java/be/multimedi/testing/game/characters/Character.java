package be.multimedi.testing.game.characters;

/**
 * @author Sven Wittoek
 * created on Wednesday, 2/10/2021
 */
public interface Character {

    public int getHealth();
    public int getRandomDamage();
    public String getName();
    public void takeDamage(int amount);
}
