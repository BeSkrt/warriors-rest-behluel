package be.multimedi.testing.game.potions;

import be.multimedi.testing.game.characters.Player;

/**
 * @author Sven Wittoek
 * created on Monday, 2/8/2021
 */
public interface Drinkable {

    void drink(Player player);
}
