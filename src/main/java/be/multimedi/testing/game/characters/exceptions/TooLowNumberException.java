package be.multimedi.testing.game.characters.exceptions;

/**
 * @author Sven Wittoek
 * created on Monday, 2/8/2021
 */
public class TooLowNumberException extends RuntimeException{
    public TooLowNumberException() {
    }

    public TooLowNumberException(String message) {
        super(message);
    }

    public TooLowNumberException(String message, Throwable cause) {
        super(message, cause);
    }

    public TooLowNumberException(Throwable cause) {
        super(cause);
    }
}
