package be.multimedi.testing.game.logic;

import be.multimedi.testing.game.characters.Character;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class CombatTest {

    private Combat combat;
    @Mock
    private Character characterToDealDamage;
    @Mock
    private Character characterToReceiveDamage;

    @BeforeEach
    void init() {
        combat = Combat.getInstance();
    }

    @Test
    void getInstance() {
        combat = Combat.getInstance();

        assertNotNull(combat);
    }

    @Test
    void fight() {
        int damage = characterToDealDamage.getRandomDamage();

        characterToReceiveDamage.takeDamage(damage);

        assertEquals(damage, characterToDealDamage.getRandomDamage());
    }

    @AfterEach
    void destroy() {
        combat = null;
    }
}