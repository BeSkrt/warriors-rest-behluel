package be.multimedi.testing.game.potions;

import be.multimedi.testing.game.characters.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

class HealthPotionTest {
    private HealthPotion smallHealthPotion;
    private HealthPotion bigHealthPotion;

    @BeforeEach
    void init() {
        smallHealthPotion = HealthPotion.SMALL_HEALTH_POTION;
        bigHealthPotion = HealthPotion.BIG_HEALTH_POTION;
    }

    @Test
    void getHealAmount() {
        assertEquals(smallHealthPotion.healAmount, smallHealthPotion.getHealAmount());
        assertEquals(bigHealthPotion.healAmount, bigHealthPotion.getHealAmount());
    }

    @Test
    void drink() {
        PlayerMock playerMock = new PlayerMock();
        playerMock.heal(smallHealthPotion.getHealAmount());

        assertTrue(playerMock.isHealCalled);
    }

    public class PlayerMock extends Player {
        boolean isHealCalled;

        @Override
        public void heal(int amount) {
            super.heal(amount);
            isHealCalled = true;
        }
    }

    @AfterEach
    void destroy() {
        smallHealthPotion = null;
        bigHealthPotion = null;
    }
}