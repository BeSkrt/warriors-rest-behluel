package be.multimedi.testing.game.logic;

import be.multimedi.testing.game.characters.Character;
import be.multimedi.testing.game.characters.Enemy;
import be.multimedi.testing.game.characters.EnemyType;
import be.multimedi.testing.game.characters.Player;
import be.multimedi.testing.game.characters.exceptions.NotFoundException;
import be.multimedi.testing.game.potions.HealthPotion;

import java.util.Random;
import java.util.Scanner;

/**
 * @author Sven Wittoek
 * created on Wednesday, 2/10/2021
 */
public class GameLogic {
    private Scanner scanner;
    private Random random;
    private int choice;
    private Player player;
    private Combat combat;

    public GameLogic(Player player) {
        this(new Scanner(System.in), player, Combat.getInstance());
    }

    public GameLogic(Scanner scanner, Player player, Combat combat) {
        setScanner(scanner);
        setCombat(combat);
        setPlayer(player);
        random = new Random();
    }

    public Scanner getScanner() {
        return scanner;
    }

     void setScanner(Scanner scanner) {
        this.scanner = scanner;
    }

    public Random getRandom() {
        return random;
    }

     void setRandom(Random random) {
        this.random = random;
    }

    public int getChoice() {
        return choice;
    }

     void setChoice(int choice) {
        this.choice = choice;
    }

    public Player getPlayer() {
        return player;
    }

     void setPlayer(Player player) {
        this.player = player;
    }

    public Combat getCombat() {
        return combat;
    }

     void setCombat(Combat combat) {
        this.combat = combat;
    }

    public void commenceCombat() {
        Enemy enemy = createEnemy();
        COMBAT_LOOP:
        while (player.getHealth() > 0 && enemy.getHealth() > 0) {
            decideAction();
            switch (choice) {
                case 1:
                    fight(enemy, player);
                    fight(player, enemy);
                    break;
                case 2:
                    if (drinkPotion()){
                        fight(enemy, player);
                    }
                    break;
                case 3:
                    fight(enemy, player);
                    break COMBAT_LOOP;
            }
        }
    }

     boolean drinkPotion() {
        try{
            player.takeOutPotion(HealthPotion.SMALL_HEALTH_POTION).drink(player);
            System.out.printf("You drank the potion healing you for %d health%n",
                    HealthPotion.SMALL_HEALTH_POTION.getHealAmount());
            return true;
        } catch (NotFoundException nfe){
            System.out.println("You don't have any potions left!");
        }
        return false;
    }

     void fight(Character characterToDealDamage, Character characterToReceiveDamage) {
        if (characterToDealDamage.getHealth() > 0 && characterToReceiveDamage.getHealth() > 0) {
            combat.fight(characterToDealDamage, characterToReceiveDamage);
        }
    }

     void decideAction() {
        System.out.printf("What would you like to do?%n" +
                "1. Fight%n" +
                "2. Drink potion%n" +
                "3. RUNNNN!!!!%n");
        choice = scanner.nextInt();
    }

     Enemy createEnemy() {
        Enemy enemy = new Enemy(EnemyType.getRandomType(), random.nextInt(player.getLevel()) + 1);
        System.out.printf("A %s appeared with %d HP. %n", enemy.getName(), enemy.getHealth());
        return enemy;
    }
}
