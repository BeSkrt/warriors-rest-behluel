package be.multimedi.testing.game.characters;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class EnemyTest {
    private Enemy goblin;
    private Enemy skeleton;

    @BeforeEach
    void init() {
        goblin = new Enemy(EnemyType.GOBLIN, 1);
        skeleton = new Enemy(EnemyType.SKELETON, 1);
    }

    @Test
    void getHealth() {
        goblin.setHealth(100);
        skeleton.setHealth(50);

        assertEquals(100, goblin.getHealth());
        assertEquals(50, skeleton.getHealth());
    }

    @Test
    @Disabled
    void getRandomDamage() {
        int randomDamageGoblin = goblin.getRandomDamage();
        int randomDamageSkeleton = skeleton.getRandomDamage();

        assertEquals(randomDamageGoblin, goblin.getRandomDamage());
        assertEquals(randomDamageSkeleton, skeleton.getRandomDamage());
    }

    @Test
    void getMaxDamage() {
        goblin.setMaxDamage(50);
        skeleton.setMaxDamage(100);

        assertEquals(50, goblin.getMaxDamage());
        assertEquals(100, skeleton.getMaxDamage());
    }

    @Test
    void getName() {
        goblin.setType("Goblin");
        skeleton.setType("Skeleton");

        assertEquals("Goblin", goblin.getName());
        assertEquals("Skeleton", skeleton.getName());
    }

    @Test
    void getLevel() {
        goblin.setLevel(10);
        skeleton.setLevel(15);

        assertEquals(10, goblin.getLevel());
        assertEquals(15, skeleton.getLevel());
    }

    @Test
    void takeDamage() {
        int damage = 20;

        goblin.takeDamage(damage);
        skeleton.takeDamage(damage);

        assertEquals(2, goblin.getHealth());
        assertEquals(0, skeleton.getHealth());
    }

    @AfterEach
    void destroy() {
        goblin = null;
        skeleton = null;
    }
}